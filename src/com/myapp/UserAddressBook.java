package com.myapp;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class UserAddressBook implements Serializable {

    private static final long serialVersionUID = 1L;

    private String name;
    private List<Friend> friends;

    public UserAddressBook(String name) {
        this(name, new ArrayList<Friend>());
    }

    public UserAddressBook(String name, List<Friend> friends) {
        this.name = name;
        this.friends = friends;
    }

    public void addFriend(Friend friend) {
        if (friends != null) {
        	friends.add(friend);
        }
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Friend> getFriends() {
        return friends;
    }

    public void setFriends(List<Friend> friends) {
        this.friends = friends;
    }

    public boolean equals(Object obj) {
        if (obj instanceof UserAddressBook) {
            UserAddressBook userAddressBook = (UserAddressBook) obj;
            return name.equals(userAddressBook.getName());
        }

        return false;
    }

    public int hashCode() {
        return (name.length());
    }
    
    //to get the unique friends from all friends
    public static Set<Friend> getUniqueFriends(List<UserAddressBook> userAddressBooks) {
        Set<Friend> commonFriends = new HashSet<Friend>();
        Set<Friend> uniqueFriends = new HashSet<Friend>();

        for (UserAddressBook userAddressBook : userAddressBooks) {
            List<Friend> friends = userAddressBook.getFriends();
            List<Friend> allFriends = new ArrayList<Friend>();
            
          
            allFriends.addAll(uniqueFriends);            
            
            allFriends.addAll(friends); 
           
            friends.retainAll(uniqueFriends); 
           
            commonFriends.addAll(friends); 
           
            allFriends.removeAll(commonFriends); 
           
            uniqueFriends.clear();
            uniqueFriends.addAll(allFriends);     
          
        }

        return uniqueFriends;

    }
}