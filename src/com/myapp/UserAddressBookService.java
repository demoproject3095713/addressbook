package com.myapp;

import java.io.EOFException;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;

public class UserAddressBookService {

    private List<UserAddressBook> addressBooks;

    public UserAddressBookService() {
        addressBooks = new ArrayList<UserAddressBook>();
        addressBooks = readAddressBooks();    
    }

    //to add friend to the addressbook
    public void addAddressBook(UserAddressBook addressbook) {
        if(!addressBooks.contains(addressbook)){
            addressBooks.add(addressbook);
            storeAddressBooks(addressBooks);
        }

    }

    //to remove friend from the addressbook
    public void removeAddressBook(UserAddressBook addressbook) {
        if(addressBooks.contains(addressbook)){
            addressBooks.remove(addressbook);
            storeAddressBooks(addressBooks);
        }

    }

    public List<UserAddressBook> getAddressBooks() {
        return addressBooks;
    }

    public void setAddressBooks(List<UserAddressBook> addressBooks) {
        this.addressBooks = addressBooks;
        storeAddressBooks(addressBooks);

    }   
    
    public void removeAllAddressBooks(){
        addressBooks.clear();
        storeAddressBooks(addressBooks);

    }   
    
    //to store the information
    public void storeAddressBooks(List<UserAddressBook> addressBooks) {
        try {
            FileOutputStream fos = new FileOutputStream("AddressBooks.txt");
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            oos.writeObject(addressBooks);
            oos.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    //to read from the previous runs
    public List<UserAddressBook> readAddressBooks() {
        List<UserAddressBook> addressBooks = new ArrayList<UserAddressBook>();
        try {
            ObjectInputStream ois = new ObjectInputStream(new FileInputStream(
                    "AddressBooks.txt"));
            if(ois.readObject() != null){
                addressBooks = (List<UserAddressBook>) ois.readObject();
            }
            ois.close();
        } catch (EOFException ex) {
            System.out.println("");
        }catch (FileNotFoundException ex) {
            System.out.println("No address books stored");
        } catch (Exception e) {
            e.printStackTrace();
        }

        return addressBooks;
    }



}