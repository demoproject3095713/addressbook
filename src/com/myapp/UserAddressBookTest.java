package com.myapp;

import static org.junit.Assert.assertTrue;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;

public class UserAddressBookTest {
    private Friend f1, f2, f3, f4, f5, f6, f7;
    private UserAddressBookService userAddressBookService;

    @Before
    public void setUp() {
    	userAddressBookService = new UserAddressBookService();
        createNewFriends();
    }


    @Test
    public void uniqueFriendsFromTwoAddressBooks() {
        System.out.println("========== List of Unique Friends from Two Address Books ==========");

        addTwoAddressBooks();

        List<UserAddressBook> addressBooks = userAddressBookService.getAddressBooks();
        printInput(addressBooks);

        // Get unique contacts from two AddressBooks
        Set<Friend> uniqueFriends = UserAddressBook.getUniqueFriends(addressBooks);

        printOutput(addressBooks, uniqueFriends);  

        // The unique contacts from these two address books should be f1 andf4
        Set<Friend> expected = new HashSet<Friend>(Arrays.asList(f1, f4));

        assertTrue(uniqueFriends.equals(expected));
    }

    @Test
    public void uniqueFriendsFromThreeAddressBooks() {
        System.out.println("==========List of Unique Friends from Three Address Books ==========");

        addThreeAddressBooks();

        List<UserAddressBook> addressBooks = userAddressBookService.getAddressBooks();
        printInput(addressBooks);       

        // Get unique contacts from three AddressBooks
        Set<Friend> uniqueContacts = UserAddressBook.getUniqueFriends(addressBooks);

        printOutput(addressBooks, uniqueContacts);

        // The unique contacts from three address books should be f4 and f5, f6, f7
        Set<Friend> expected = new HashSet<Friend>(Arrays.asList(f4, f5, f6, f7));

        assertTrue(uniqueContacts.equals(expected));
    }

    @Test
    public void sortFriendsByTheirNames() {
        System.out.println("========== List of Friends Sorted by Their Names ==========");

        UserAddressBook addressBook = new UserAddressBook("ab1");
        addressBook.addFriend(f3);
        addressBook.addFriend(f5);
        addressBook.addFriend(f1);
        addressBook.addFriend(f4);
        addressBook.addFriend(f2);        


        System.out.println("==Input==");
        System.out.println("Address Book: " + addressBook.getName());
        System.out.println("Friends:");
        for (Friend friends : addressBook.getFriends()) {
            System.out.println(friends);
        }
        System.out.println();

        Collections
                .sort(addressBook.getFriends(), new FriendNameComparator());      
        
        System.out.println("==Output==");
        System.out.println("Address Book: " + addressBook.getName());
        System.out.println("Friends:");
        for (Friend friends : addressBook.getFriends()) {
            System.out.println(friends);
        }
        System.out.println("\n");

    }

    private void createNewFriends() {
        f1 = new Friend("Bob", "9876543210");
        f2 = new Friend("Mary", "9999988888");
        f3 = new Friend("Jane", "9123456780");
        f4 = new Friend("John", "9999911111");
        f5 = new Friend("Lio", "9222222221");
        f6 = new Friend("Rio", "9090909090");
        f7 = new Friend("Simi", "8282828282");
    }
    
    // Add Friends to The Two Address Books
    private void addTwoAddressBooks() {
    	userAddressBookService.removeAllAddressBooks();

        UserAddressBook ab1 = new UserAddressBook("ab1");
        UserAddressBook ab2 = new UserAddressBook("ab2");
       
        ab1.addFriend(f1);
        ab1.addFriend(f2);
        ab1.addFriend(f3);

        ab2.addFriend(f2);
        ab2.addFriend(f4);
        ab2.addFriend(f3);

        userAddressBookService.addAddressBook(ab1);
        userAddressBookService.addAddressBook(ab2);
    }

    // Add Friends to The Three Address Books
    private void addThreeAddressBooks() {
    	userAddressBookService.removeAllAddressBooks();

        UserAddressBook ab1 = new UserAddressBook("ab1");
        UserAddressBook ab2 = new UserAddressBook("ab2");
        UserAddressBook ab3 = new UserAddressBook("ab3");
       
        ab1.addFriend(f1);
        ab1.addFriend(f2);
        ab1.addFriend(f3);

        ab2.addFriend(f2);
        ab2.addFriend(f4);
        ab2.addFriend(f3);

        ab3.addFriend(f1);
        ab3.addFriend(f5);
        ab3.addFriend(f6);
        ab3.addFriend(f7);

        userAddressBookService.addAddressBook(ab1);
        userAddressBookService.addAddressBook(ab2);
        userAddressBookService.addAddressBook(ab3);

    }
    
    
    //print the friends names according tp the address books
    private void printInput(List<UserAddressBook> addressBooks){
        System.out.println("==Input==");

        for (UserAddressBook addressBook : addressBooks) {
            System.out.println("Address Book: " + addressBook.getName());
            System.out.println("Friends:");
            for (Friend f : addressBook.getFriends()) {
                System.out.println(f.getName());
            }
            System.out.println("");

        }
    }

    //print the unique friends names from the all address books
    private void printOutput(List<UserAddressBook> addressBooks,Set<Friend> uniqueFriends){
        System.out.println("==Output==");
        System.out.print("Address Books: ");
        String names = "";
        for (UserAddressBook addressBook : addressBooks) {
            names += addressBook.getName() + ", ";
        }

        if (names.length() > 0) {
            System.out.println(names.substring(0, names.lastIndexOf(",")));
        }
        for (Friend f : uniqueFriends) {
            System.out.println(f.getName());
        }
        System.out.println("\n");
    }

}