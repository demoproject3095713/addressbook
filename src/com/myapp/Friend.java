package com.myapp;

import java.io.Serializable;
import java.util.Comparator;

public class Friend implements Serializable {

    private static final long serialVersionUID = 1L;

    private String name;
    private String phoneNumber;

    public Friend(String name, String phoneNumber) {
        this.name = name;
        this.phoneNumber = phoneNumber;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String toString() {
        return name + ", " + phoneNumber;
    }

    public boolean equals(Object obj) {
        if (obj instanceof Friend) {
            Friend friend = (Friend) obj;
            return (name.equals(friend.getName()) && phoneNumber
                    .equals(friend.getPhoneNumber()));
        }

        return false;
    }

    public int hashCode() {
        return (name.length() + phoneNumber.length());
    }
    
 }

//creates the comparator for comparing friends names
class FriendNameComparator implements Comparator<Friend> {
	//override the compare() method
    public int compare(Friend friend1, Friend friend2) {
    	    	
        return friend1.getName().compareToIgnoreCase(friend2.getName());         
    }
}